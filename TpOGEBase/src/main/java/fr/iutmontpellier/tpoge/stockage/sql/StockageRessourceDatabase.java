package fr.iutmontpellier.tpoge.stockage.sql;

import fr.iutmontpellier.tpoge.metier.entite.Ressource;
import fr.iutmontpellier.tpoge.stockage.Stockage;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StockageRessourceDatabase implements Stockage<Ressource> {
    @Override
    public void create(Ressource element) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String req = "INSERT INTO RessourcesOGE (nom) VALUES (?)";
        try (PreparedStatement statement = connection.prepareStatement(req))
        {
            statement.setString(1, element.getNom());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Ressource element) {
            SQLUtils utils = SQLUtils.getInstance();
            Connection connection = utils.getConnection();
            String req = "UPDATE RessourcesOGE SET nom = ? WHERE idRessource = ?";
            try (PreparedStatement statement = connection.prepareStatement(req))
            {
                statement.setString(1,element.getNom());
                statement.setInt(2, element.getIdRessource());
                statement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    @Override
    public void deleteById(int id) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String req = "DELETE FROM RessourcesOGE WHERE idRessource = ?";
        try (PreparedStatement statement = connection.prepareStatement(req))
        {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Ressource getById(int id) {
        Ressource ressource = null;
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String requete = "SELECT idRessource, nom FROM RessourcesOGE WHERE idRessource = " + id ;
        try (
                Statement st = connection.createStatement();
                ResultSet result = st.executeQuery(requete);
        )
        {
            result.next() ; //On accède à la prochaine ligne
            int idRes = result.getInt( 1);
            String nom = result.getString( 2);
            ressource = new Ressource(nom) ;
            ressource.setIdRessource(id);
        } catch (SQLException e) {
            e.printStackTrace() ;
        }
        return ressource;
    }

    @Override
    public List<Ressource> getAll() {
        List<Ressource> listeRessource = new ArrayList<>();
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String req = "SELECT idRessource, nom FROM RessourcesOGE";
        try (
                Statement st = connection.createStatement();
                ResultSet result = st.executeQuery("SELECT idRessource, nom FROM RessourcesOGE");
        )
        {
            while(result.next()) {
//On accède à la prochaine ligne
                int id = result.getInt(1);
                String nom = result.getString(2);
                Ressource res = new Ressource(nom);
                res.setIdRessource(id);
                listeRessource.add(res);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listeRessource;
    }
}
