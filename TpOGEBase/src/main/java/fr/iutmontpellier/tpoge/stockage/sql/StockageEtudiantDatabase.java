package fr.iutmontpellier.tpoge.stockage.sql;

import fr.iutmontpellier.tpoge.application.service.RessourceService;
import fr.iutmontpellier.tpoge.metier.entite.Etudiant;
import fr.iutmontpellier.tpoge.metier.entite.Ressource;
import fr.iutmontpellier.tpoge.stockage.Stockage;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
public class StockageEtudiantDatabase implements Stockage<Etudiant>{

    @Override
    public void create(Etudiant element) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String req = "INSERT INTO EtudiantsOGE (nom,prenom,idRessourceFavorite) VALUES (?,?,?)";
        try (PreparedStatement statement = connection.prepareStatement(req))
        {
            statement.setString(1, element.getNom());
            statement.setString(2,element.getPrenom());
            statement.setInt(3,element.getRessourceFavorite().getIdRessource());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Etudiant element) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String req = "UPDATE EtudiantsOGE SET nom = ?, prenom = ?, idRessourceFavorite = ?";
        try (PreparedStatement statement = connection.prepareStatement(req)) {
            statement.setString(1, element.getNom());
            statement.setString(2, element.getPrenom());
            statement.setInt(3, element.getRessourceFavorite().getIdRessource());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String req = "DELETE FROM EtudiantsOGE WHERE idEtudiant = ?";
        try (PreparedStatement statement = connection.prepareStatement(req)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Etudiant getById(int id) {
        Etudiant etudiant = null;
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String requete = "SELECT idEtudiant, nom,prenom,idRessourceFavorite FROM EtudiantsOGE WHERE idEtudiant = " + id ;
        try (
                Statement st = connection.createStatement();
                ResultSet result = st.executeQuery(requete);
        )
        {
            result.next() ; //On accède à la prochaine ligne
            int idEtu = result.getInt( 1);
            String nom = result.getString( 2);
            String prenom=result.getString(3);
            int idResFav=result.getInt(4);
            etudiant= new Etudiant(nom,prenom, RessourceService.getInstance().getRessource(idResFav)) ;
            etudiant.setIdEtudiant(idEtu);
        } catch (SQLException e) {
            e.printStackTrace() ;
        }
        return etudiant;
    }

    @Override
    public List<Etudiant> getAll() {
        List<Etudiant> listeEtudiant = new ArrayList<>();
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String req = "SELECT idEtudiant,nom,prenom,idRessourceFavorite FROM EtudiantsOGE";
        try (
                Statement st = connection.createStatement();
                ResultSet result = st.executeQuery(req);
        )
        {
            while(result.next()) {
//On accède à la prochaine ligne
                int id = result.getInt(4);
                String nom = result.getString(2);
                String prenom = result.getString(3);
                Etudiant etu=new Etudiant(nom,prenom,RessourceService.getInstance().getRessource(id));
                etu.setIdEtudiant(result.getInt(1));
                listeEtudiant.add(etu);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listeEtudiant;
    }
}
